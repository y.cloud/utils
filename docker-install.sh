#!/bin/sh

#################################################
# Mainteiner "Evgenii Gezha <egezha@gmail.com>" #
#################################################

# Check current user
[ $(whoami) != "root" ] && { echo "ERROR!!! Only root can run that."; exit 1; }

h_name=$(hostname -s)

# Update the apt package index
apt-get update

#####
# Installing docker
# Reference guidge https://docs.docker.com/install/linux/docker-ce/ubuntu/
#####

# Install packages to allow apt to use a repository over HTTPS
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Setting up the stable repository
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update

# Installing CE edition
apt-get install -y docker-ce

# Change dockerd config
# WARNING: config file doesn't support any comments
# Reference guidge https://docs.docker.com/engine/reference/commandline/dockerd/

cat > /etc/docker/daemon.json << EOF
{
	"debug": false,
	"selinux-enabled": false,
	"mtu": 1450,
	"data-root": "/u0/docker"
}
EOF

# Apply dockerd config
service docker restart

# Add system user ubuntu to the docker group for run docker containers with root privileges
usermod -aG docker ubuntu

# Verify that Docker CE is installed correctly by running the hello-world image
docker run hello-world


#####
# Installing docker-compose
# Reference guidge https://docs.docker.com/compose/install/
#####

# Download the latest version of Docker Compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Apply executable permissions to the binary
chmod +x /usr/local/bin/docker-compose

# Test the installation
docker-compose --version


#####
# Installing GitLab
# Reference guidge https://docs.gitlab.com/runner/install/linux-repository.html
#####

# Add GitLab's official repository
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# Install the latest version of GitLab Runner
# After installation create user gitlab-runner and home dir /home/gitlab-runner
apt-get install -y gitlab-runner

